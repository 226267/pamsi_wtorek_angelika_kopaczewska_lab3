#include<iostream>
#include<cstdlib>
#include<stack>

using namespace std;

template<typename T>
class StosSTL
{
private:
    stack<T> st;
public:
    T& top()
	{
        return st.back();
	}

	bool isEmpty()  // sprawdzanie czy pusty
	{
	    return st.empty();
	}

	int getsize()   // pobeiranie rozmiaru
	{
	    return st.size();
	}

	void Dodaj(T el)  // dodawanie elementu
	{
	    st.push(el);
	}

	T Usun()   // usuwanie elementu
	{
	    if(st.empty())
            cout<<"Stos jest pusty i nie mozna usunac z niego elementu"<<'\n';
        else
            st.pop();
	}

	void UsunWszystko()  // cyszczenie stosu
	{

            while (!st.empty())
                st.pop();
	}

	void Wyswietl()
	{
	     if(st.empty())
            cout<<"Stos jest pusty i nie mozna usunac z niego elementu"<<'\n';
            else{
                stack<T> tmp;
                while(!st.empty())
                {
                    cout<<st.top()<<endl;
                    tmp.push(st.top());
                    st.pop();
                }

                while(!tmp.empty())
                {
                    st.push(tmp.top());
                    tmp.pop();
                }

	}
	}

};

int main()
{



     cout<<endl;
    cout<<"-----MENU-----"<<endl;
    cout<<"1. Dodaj element do listy"<<endl;
    cout<<"2. Usun jeden element z listy"<<endl;
    cout<<"3. Wyswietl zawartosc listy"<<endl;
    cout<<"4. Usun wszystkie elementy z listy"<<endl;
    cout<<"0. Powrot do menu"<<endl<<endl;

StosSTL<int> S;

int el;
char opt;

do
    {
        cout<<"Podaj opcje: ";
		cin>>opt;
		cout<<endl;

		switch (opt)
		{
        case '1':
            cout<<"podaj el: "<<'\n';
            cin>>el;
           S.Dodaj(el);
            break;

        case'2':
            S.Usun();
            break;

       case '3':
           S.Wyswietl();
           break;

       case '4':
           S.UsunWszystko();
        break;



        case '0':
            cout<<"Koniec programu."<<endl;
            break;

        default:
            cout<<"Wybrales zly numer. Sprobuj ponownie"<<endl;
		}
	}
	while (opt != '0');

return 0;

    cin.get();
}

